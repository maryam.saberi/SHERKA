# نرم افزار حقوق و دستمزد شرکت کود وسم
به کمک این نرم افزار حقوق هر فرد با توجه به ساعات کاری اش محاسبه شده و کارمندان میتوانند به صورت غیرحضوری و از طریف این نرم افار فیش حقوقی خوذ را دریافت نماید

## ویژگی های کلیدی نرم افزار حقوق و دستمزد شرکت کودو سم
* محاسبه حقوق و اضافه حقوق کارمندان
* دریافت فیش حقوقی توسط کارمندان بدون مراجعه حضوری
* کاهش خطا و اشتباه در محاسبه حقوق


## تحلیل و طراحی پروژه
* در ابتدا به بررسی چند عملکرد اصلی در نرم افزار در قالب سناریو کارمندان و حسابدار  پرداخته ایم.[سناریو ها](DOCUMENTATION/SCENARIO.md)
* مدل سازی سناریو با استاده از [نمودار مورد استفاده](DOCUMENTATION/USECASE.md)
* [نیازمندی های پروژه](DOCUMENTATION/REQUIRMENT.md)
* [seq diagram](DOCUMENTATION/images/sequence)
* [class diagram](DOCUMENTATION/images/ClassDiagram.Png)
* 
 
 ## فاز های توسعه پروژه
 * صفحه ورود به نرم افزار
 * صفحه ثبت و حذف کارمندان توسط حسابدار
 * صفحه نمایش فیش حقوقی
 * صفحه ساعات ورودی و خروجی و اضافه کاری
 
 
 ##توسعه دهندگان:

|نام و نام خانوادگی|   ID     |
|:----------------:|:--------:|
|     زینب طاهری     | @zeynabtaheri|
|     مریم صابری     | @maryam.saberi|