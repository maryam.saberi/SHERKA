﻿using ERIS_USERS;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sherkat
{
    public partial class CalcPrice : Form
    {
        public CalcPrice()
        {
          
            InitializeComponent();

        }
        DataView dvEMPM = new DataView();
        DataView dvEmps = new DataView();
        Db db = new Db();
        bool bEMPM;
        private void button1_Click(object sender, EventArgs e)
        {
            
            string strcmd = "select LastName as [نام خانوادگی] , Name as [نام]  from employees where PersoneliCode ='" + this.textBox1.Text + "'";
            dvEmps = db.GetView(strcmd);

            try
            {
                dataGridView1.DataSource = dvEmps;
                txtFamily.Text = dataGridView1.Rows[0].Cells[0].Value.ToString();
                txtName.Text = dataGridView1.Rows[0].Cells[1].Value.ToString();

            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            //string code = textBox1.Text;
            //if (code == "101")
            //{
            //    lblName.Text = "مریم";
            //    lblFamily.Text = "صابری";
            //}
            //else if (code == "102")
            //{
            //    lblName.Text = "زینب";
            //    lblFamily.Text = "طاهری";
            //}
            //else
            //{
            //    MessageBox.Show("کد پرسنلی وارد شده صحیح نمی باشد", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }


        private void button2_Click(object sender, EventArgs e)
        {
            if (txtPaye.Text != "" && txtEzafe.Text != "" && txtOff.Text != "" && txtUnit.Text != "")
            {
                double paye = Convert.ToDouble(txtPaye.Text);
                double ezafe = Convert.ToDouble(txtEzafe.Text);
                double off = Convert.ToDouble(txtOff.Text);
                double unit = Convert.ToDouble(txtUnit.Text);

                double total = paye + (ezafe * unit) - (off * unit);

                txtTotal.Text = total.ToString();

                MessageBox.Show("حقوق با موفقیت محاسبه شد", "محاسبه شد", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("لطفا همه فیلدها را پر کنید", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            // int baseslary;
            int overtime;
            int leavetime;
            // int price;
            int salary;
            // string sbasesalary;
            string sovertime;
            string sleavetime;
            // string sprice;
            string ssalary;
            // bool b = int.TryParse(txtPaye.Text, out baseslary);
            bool O = int.TryParse(txtEzafe.Text, out overtime);
            bool L = int.TryParse(txtOff.Text, out leavetime);
            // bool P = int.TryParse(txtUnit.Text, out price);
            bool S = int.TryParse(txtTotal.Text, out salary);
            
            //if (b)
            //{
            //    sbasesalary = baseslary.ToString();
            //}
            //else
            //{
            //    sbasesalary = "NULL";
            //}
            string spersonelycode=textBox1.Text.ToString();
            if (O)
            {
                sovertime = overtime.ToString();
            }
            else
            {
                sovertime = "null";
            }
            if (L)
            {
                sleavetime = leavetime.ToString();
            }
            else
            {
                sleavetime = "null";
            }
            //if (P)
            //{
            //   sprice = price.ToString();
            //}
            //else
            //{
            //    sprice = "null";
            //}
            if (S)
            {
                ssalary = salary.ToString();
            }
            else
            {
                ssalary = "null";
            }

            string strcmd = "if  exists (select PersoneliCode,Date from  salary  where( PersoneliCode=" + spersonelycode + " AND Date='" + datecombo.Text + "')) update salary set  OTime=" + sovertime + ", LTime=" + sleavetime + ", salary=" + ssalary + " where( PersoneliCode=" + spersonelycode + " AND Date='" + datecombo.Text + "') else if not exists (select PersoneliCode,Date from  salary  where( PersoneliCode=" + spersonelycode + " AND Date='" + datecombo.Text + "'))  insert salary (PersoneliCode,Otime,Ltime,salary,Date)" + "values (" + textBox1.Text + ", " + sovertime + ", " + sleavetime + ", " + ssalary + ",'" + datecombo.Text + "' )  ";

            try
            {
                bEMPM = db.RunCommand(strcmd);
                MessageBox.Show("اطلاعات با موفقیت در دیتابیس ذخیره شدند", "ذخیره موفق", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);

            }
            finally
            {
                //con.Close();
            }
           
        }

        private void CalcPrice_Load(object sender, EventArgs e)
        {

        }
    }
}
