﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;


namespace ERIS_USERS
{
    class Db
    {
        string connectionString = "";
        SqlConnection con;
        SqlDataAdapter da;
        SqlCommand cmd;
        //SqlCommandBuilder cb;
        DataSet ds = new DataSet();
        DataView dv = new DataView();
        DataTable dt = new DataTable();

        public Db()
        {
            connectionString = Sherkat.Properties.Settings.Default["connection"].ToString();
            con = new SqlConnection(connectionString);
        }

        public Db(string conString)
        {
            connectionString = conString;
            con = new SqlConnection(connectionString);
        }
        //برقراری ارتباط
        public bool IsConnected(out string ErrorMessage)
        {
            try
            {
                con.Open();
                con.Close();
                ErrorMessage = "";
                return true;
            }
            catch (System.Data.SqlClient.SqlException err)
            {
                ErrorMessage = err.Message;
                con.Close();
                return false;
            }
        }
        //نمایش اطلاعات در datareader(برای دستورات select)
        public DataView GetView(string strSel)
        {
            ds.Clear();
            //strsel دستور select 
            da = new SqlDataAdapter(strSel, con);
            da.Fill(ds, "tbl");
            dv = ds.Tables["tbl"].DefaultView;
            return dv;
        }
        public DataTable GetView3(string strSel)
        {
            ds.Clear();
            //strsel دستور select 
            da = new SqlDataAdapter(strSel, con);
            da.Fill(ds, "tbl");
            dt = ds.Tables["tbl"];
            return dt;
        }
        //insert,update,delete برای دستورات برای حذف کارمند
        public bool RunCommandEMPM(string strCmd)
        {
            cmd = new SqlCommand(strCmd, con);
            ds.Clear();
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (SqlException es)
            {
                MessageBox.Show("لطفا ابتدا نقش های کارمند را حذف نمایید", "خطا",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
                return false;
            }
        }

        //متد برای دستورات insert,update,delete
        public bool RunCommand(string strCmd)
        {
            cmd = new SqlCommand(strCmd, con);
            ds.Clear();
            try
            {
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (SqlException es)
            {
                MessageBox.Show(es.Message, "خطا",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
                return false;
            }
        }

        //متد دستورات insert,update,delete با مقدار بازگشتی مانند قسمت ختصاص نقش به کارمندان
        public int RunCommandScalar(string strCmd)
        {
            cmd = new SqlCommand(strCmd, con);
            try
            {
                con.Open();
                object result = cmd.ExecuteScalar();
                con.Close();
                return int.Parse(result.ToString());
            }
            catch (SqlException err)
            {
                MessageBox.Show(err.Message, "خطا",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                con.Close();
                return -1;
            }
        }


    }
}

