﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Sherkat
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoginPrice lp = new LoginPrice();
            lp.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            reportSalary rs = new reportSalary();
            rs.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ShowSalary ss = new ShowSalary();
            ss.ShowDialog();
        }
    }
}
